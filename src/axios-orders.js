import axios from 'axios' ;

const instance = axios.create({
    baseURL : 'https://react-my-burger-7b893.firebaseio.com/'
});


export default instance;