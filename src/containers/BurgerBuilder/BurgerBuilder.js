import React, { Component } from 'react'; 
import { connect } from 'react-redux';

import Aux from '../../hoc/Auxx/Aux';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axios-orders';
import* as actionTypes from '../../store/actions';
 
const INGREDIENT_PRICES = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.7
}

class BurgerBuilder extends Component {

    // constructor(props){
    //     super(props);

    //     this.state = { }
    // }

    state = {
        ingredients : null,
        totalPrice : 4,
        purchaseable: false,
        purchasing: false,
        loading : false,
        error: false
    }

    componentDidMount(){

        console.log(this.props);

        axios.get('https://react-my-burger-7b893.firebaseio.com/ingredients.json')
        .then( response => {
            this.setState({ ingredients: response.data })
        }).catch(error => {
            this.setState({ error : true })
        });
    }

    updatePurchaseState(ingredients     )
    {
        // const ingredients = { 
        //     ...this.state.ingredients
        // };

        const sum = Object.keys(ingredients)
        .map(igKey =>{
            return ingredients[igKey];
        })
        .reduce((sum, el)=> {
            return sum + el;
        },0);

        this.setState({ purchaseable: sum > 0 })
    }

    addIngredientHandler = (type ) => {
        const oldCount = this.state.ingredients[type];
        const updatedCount = oldCount + 1;
        const updatedIngredients = {
            ...this.state.ingredients
        };
        updatedIngredients[type] = updatedCount; 
        const priceAddition = INGREDIENT_PRICES[type];
        const oldPrice = this.state.totalPrice ;
        const newPrice = oldPrice + priceAddition;
        this.setState({ totalPrice: newPrice, ingredients: updatedIngredients});
        this.updatePurchaseState(updatedIngredients);
    }

    removeIngredientHandler = (type ) => {
        const oldCount = this.state.ingredients[type];

        if(oldCount <= 0)
        {
            return;
        }
        const updatedCount = oldCount - 1;
        const updatedIngredients = {
            ...this.state.ingredients
        };
        updatedIngredients[type] = updatedCount; 
        const priceDeduction = INGREDIENT_PRICES[type];
        const oldPrice = this.state.totalPrice ;
        const newPrice = oldPrice - priceDeduction;
        this.setState({ totalPrice: newPrice, ingredients: updatedIngredients});
        this.updatePurchaseState(updatedIngredients); 
    }

    purchaseHandler = () => 
    {
        this.setState({purchasing: true })
    }

    purchaseCancellHandler = () =>{
        this.setState({purchasing: false });
    }

    purchaseContinueHandler =() =>{
        const queryParam = [];

        for(let i in this.state.ingredients){
            queryParam.push(encodeURIComponent(i) + '=' + encodeURIComponent(this.state.ingredients[i]))
        }

        queryParam.push('Price = ' + this.state.totalPrice);
        const queryString = queryParam.join('&');
        this.props.history.push({
            pathname: '/checkout',
            search: '?' + queryString 
        });
    }

    render(){

        const disabledInfo = {
            ...this.state.ings
        };
 
        for(let key in disabledInfo)
        {
            disabledInfo[key] = disabledInfo[key] <= 0  
        }

        let orderSummary = null;
        let burger  = this.state.error ?
                <p>Ingredients can't be loaded</p> : <Spinner/>;

        if(this.props.ings)
        {
            burger  = (
                <Aux>
                     <Burger  ingredients = {this.this.props }/>
                    <BuildControls 
                        ingredientAdded = { this.props.onIngredientAdded }
                        ingredientRemoved = { this.props.onIngredientRemoved }
                        disabled ={disabledInfo }
                        purchaseable = {this.state.purchaseable}
                        ordered = { this.purchaseHandler }
                        price = {this.state.totalPrice  } />
                </Aux>
            );
 
            orderSummary =  <OrderSummary 
                    ingredients = {this.this.ings } 
                    price = {this.state.totalPrice}
                    purchaseCancelled = {this.purchaseCancellHandler}
                    purchaseContinued = {this.purchaseContinueHandler}
                />;
        }
        if(this.state.loading)
        {
            orderSummary = <spinner/>;
        }

        return( 
            <Aux>
                <Modal show= { this.state.purchasing } 
                    modalClosed = { this.purchaseCancellHandler }>
                   { orderSummary }
                 </Modal>  
                 { burger }
            </Aux>
        );
    } 
} 

const mapStateToProps = state => {
    return {
        ings: state.ingredients
    };
}

const mapDispatchToProps = dispatch=>{
    return {
        onIngredientAdded: (ingName) => dispatch({ type: actionTypes.ADD_INGREDIENT, ingredientName: ingName }),
        onIngredientRemoved: (ingName) => dispatch({ type: actionTypes.REMOVE_INGREDIENT, ingredientName: ingName })
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (withErrorHandler(BurgerBuilder,axios));